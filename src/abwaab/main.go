package main

import (
	"database/sql"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/dghubble/go-twitter/twitter"
	"github.com/dghubble/oauth1"
	"github.com/golang-jwt/jwt"
	"github.com/jinzhu/gorm"
	"io/ioutil"
	"log"
	"net/http"
	"net/mail"
	"time"
)

import (
	_ "github.com/golang-jwt/jwt"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "io/ioutil"
	_ "sync"
)

// User defined data type **/
type User struct {
	gorm.Model
	Email string  `json:"email" validate:"required"`
	Password string `json:"password" validate:"required"`
}
// AbwaabTweet defined data type **/
type AbwaabTweet struct {
	gorm.Model
	Description string `json:"description" validate:"required"`
}
// Credentials defined data type **/
type Credentials struct {
	Email string `json:"email" validate:"required"`
	Password string `json:"password" validate:"required"`
}
// DBParam defined data type **/
type DBParam struct {
	Limit string `json:"limit"`
	Offset string `json:"offset"`
}
// Claims defined data type **/
type Claims struct {
	Email string `json:"email"`
	jwt.StandardClaims
}

// Variables to be used across all over REST. This will work instead of session mechanism **/
// TODO There's a need to apply session mechanism in order to use the data between different endpoints.
// TODO More understanding for Go is required especially for those variables that came shared between different endpoints
var (AbwaabTweets [] AbwaabTweet
	privateKey, _ = ioutil.ReadFile("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiU2VuaW9yIFNvZnR3YXJlIEVuZ2luZWVyIiwibmFtZSI6IkpvaG4gRG9lIn0.KPq0QAfOAXF0W8qCceQUBz1JOAP4arMv3yHWl0CXk5Y")
	db *gorm.DB
	client *twitter.Client
	err error
	rows *sql.Rows
	cookie *http.Cookie
	tkn *jwt.Token)
/**
Name: valid
Param: string
Purpose: Check whether email is valid or not
**/
func valid(email string) bool {
	_, err := mail.ParseAddress(email)
	return err == nil
}
/**
Name: encodeBase64
Param: [] bytes
Purpose: Encode user password
**/
func encodeBase64(b []byte) string {
	return base64.StdEncoding.EncodeToString(b)
}
/**
Name: decodeBase64
Param: [] bytes
Purpose: Decode user password
**/
func decodeBase64(s string) []byte {
	data, err := base64.StdEncoding.DecodeString(s)
	if err != nil { panic(err) }
	return data
}
/**
Name: init
Param:
Purpose: Init Go Callback
**/
func init(){
	initTwitter()
	initDatabase()
}
/**
Name: initTwitter
Param:
Purpose: Init Twitter Tokens
**/
func initTwitter(){
	config := oauth1.NewConfig("btLGY4ZqbS7slThCH7ZwQLELd", "04dvtBVob9pv5yMSELPgZEFgHBxhefEdmRjlNHIUVPQKXlT6uP")
	token := oauth1.NewToken("753752674810654721-0qnxQE53JEYFAWzW3ynErBPtghvX2c3", "0Q5J9vywb1i3xaW586IG8UU7xezdBYPX63LE4Uf64kfmO")
	httpClient := config.Client(oauth1.NoContext, token)
	client = twitter.NewClient(httpClient)
}
/**
Name: initDatabase
Param:
Purpose: Init Database
**/
func initDatabase(){
	db, err = gorm.Open( "postgres", "host=localhost port=5432 dbname=postgres sslmode=disable")
	if err != nil {
		panic("failed to connect database")
	}
}
/**
Name: getTweets
Param: Response, Request
Purpose: Get all tweets from timeline
**/
func getTweets(w http.ResponseWriter, r *http.Request){
	if !checkTokenValidity(w,r){
		return
	}
	tweets, _, err := client.Timelines.HomeTimeline(&twitter.HomeTimelineParams{
		Count: 5,
	})

	if err != nil {
		fmt.Println("Failed to execute getTweets: ", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	for _, s := range tweets {
		var tweet AbwaabTweet = AbwaabTweet{
			Description: s.Text,
		}
		AbwaabTweets = append(AbwaabTweets,tweet)
	}
}
/**
Name: createTweet
Param: Response, Request
Purpose: Create tweet into database directly
**/
func createTweet(w http.ResponseWriter, r *http.Request) {
	if !checkTokenValidity(w,r){
		return
	}
	var abwaabTweet AbwaabTweet
	err := json.NewDecoder(r.Body).Decode(&abwaabTweet)
	if err != nil {
		fmt.Println("Failed to execute createTweet: ", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	db.AutoMigrate(&AbwaabTweet{})
	db.Create(&abwaabTweet)
}
/**
Name: saveCurrentFetchedTweets
Param: Response, Request
Purpose: Save current fetched tweets
**/
func saveCurrentFetchedTweets(w http.ResponseWriter, r *http.Request){
	if !checkTokenValidity(w,r){
		return
	}
	// TODO Fetched tweets have to be acquired from session-based mechanism
	if len(AbwaabTweets) == 0 {
		fmt.Println("No AbwaabTweets To Save ::")
		w.WriteHeader(http.StatusAccepted)
		return
	}
	db.AutoMigrate(&AbwaabTweet{})
	for index := range AbwaabTweets {
		db.Create(&AbwaabTweets[index])
	}
	// Empty
	AbwaabTweets = nil
}
/**
Name: navigateTweets
Param: Response, Request
Purpose: Surfacing tweets from database with pagination capability, limit and offset are required
**/
func navigateTweets(w http.ResponseWriter, r *http.Request){
	if !checkTokenValidity(w,r){
		return
	}
	// Reset list of results
	AbwaabTweets = nil
	var dbparam DBParam
	err := json.NewDecoder(r.Body).Decode(&dbparam)
	if err != nil {
		fmt.Println("Failed to execute navigation :: ", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	rows, err = db.DB().Query("SELECT Description FROM ABWAAB_TWEETS ABT LIMIT $1 OFFSET $2", dbparam.Limit, dbparam.Offset)
	if err != nil {
		fmt.Println("Failed to execute query :: ", err)
	}

	for rows.Next() {
		var abwaabTweet AbwaabTweet
		rows.Scan(&abwaabTweet.Description)
		AbwaabTweets = append(AbwaabTweets, abwaabTweet)
	}

	json.NewEncoder(w).Encode(&AbwaabTweets)
}
/**
Name: getUser
Param: string
Purpose: Get User by email
**/
func getUser(email string) User {
	var user User
	rows, err := db.DB().Query("SELECT email, password FROM USERS WHERE email=$1", email)
	if err != nil {
		fmt.Println("Failed to execute query: ", err)
	}
	for rows.Next() {
		rows.Scan(&user.Email, &user.Password)
		break
	}
	return user
}
/**
Name: register
Param: Response, Request
Purpose: Register user
**/
func register(w http.ResponseWriter, r *http.Request) {
	var credentials Credentials
	err := json.NewDecoder(r.Body).Decode(&credentials)
	if err != nil {
		fmt.Println("Failed to execute register: ", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if !valid(credentials.Email) {
		fmt.Println("Wrong email")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	u := getUser(credentials.Email)

	if u.Email != "" {
		fmt.Println("User already exist")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	db.AutoMigrate(&User{})

	user := User {
		Email: credentials.Email,
		Password: encodeBase64([]byte(credentials.Password)),
	}

	db.Create(&user)
}
/**
Name: login
Param: Response, Request
Purpose: Provide users of REST services the needed authentication
**/
func login(w http.ResponseWriter, r *http.Request){
	var credentials Credentials
	// TODO There's possibility to use github.com/go-playground/validator/v10 for struct validation
	err := json.NewDecoder(r.Body).Decode(&credentials)
	if err != nil {
		fmt.Println("Bad request ::")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	user := getUser(credentials.Email)
	if &user == nil {
		fmt.Println("Wrong email or password ::")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if string(decodeBase64(user.Password)) != credentials.Password {
		fmt.Println("Wrong email or password ::")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	expirationTime := time.Now().Add(time.Minute * 2)
	claims := &Claims{
		Email: credentials.Email,
		StandardClaims : jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(privateKey)
	if err != nil {
		fmt.Println("Unable to create token ::")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	http.SetCookie(w,&http.Cookie{
		Name: "token",
		Value: tokenString,
		Expires: expirationTime,
	})
}
/**
Name: checkTokenValidity
Param: Response, Request
Purpose: Check if the given token was valid or not
**/
func checkTokenValidity(w http.ResponseWriter, r *http.Request) bool{
	cookie, err = r.Cookie("token")
	if err != nil {
		if err == http.ErrNoCookie {
			fmt.Println("User Unauthorized ::")
			w.WriteHeader(http.StatusUnauthorized)
			return false
		}
	}

	tokenStr := cookie.Value
	claims := &Claims{}

	tkn, err = jwt.ParseWithClaims(tokenStr,claims, func(token *jwt.Token) (interface{}, error) {
		return privateKey,nil
	})

	if err != nil {
		if err == jwt.ErrSignatureInvalid {
			fmt.Println("User Unauthorized ::")
			w.WriteHeader(http.StatusUnauthorized)
			return false
		}
		w.WriteHeader(http.StatusBadRequest)
		return false
	}

	if !tkn.Valid {
		fmt.Println("User Unauthorized ::")
		w.WriteHeader(http.StatusUnauthorized)
		return false
	}

	return true
}
/**
Name: handleRequests
Param:
Purpose: Register reset endpoints
**/
func handleRequests(){
	http.HandleFunc("/login", login)
	http.HandleFunc("/register", register)
	http.HandleFunc("/getTweets", getTweets)
	http.HandleFunc("/saveCurrentFetchedTweets", saveCurrentFetchedTweets)
	http.HandleFunc("/navigateTweets", navigateTweets)
	http.HandleFunc("/createTweet", createTweet)
	log.Fatal(http.ListenAndServe(":8081", nil))
}
/**
Name: main
Param:
Purpose: Start method in Go
**/
func main(){
	handleRequests()
}
