package main

import (
	"github.com/golang-jwt/jwt"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"
)

func doAuthForTest(request *http.Request){
	expirationTime := time.Now().Add(time.Minute * 2)
	claims := &Claims{
		Email: "mohd.amr@hotmail.com",
		StandardClaims : jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, _ := token.SignedString(privateKey)
	request.AddCookie(&http.Cookie{
		Name: "token",
		Value: tokenString,
		Expires: expirationTime,
	})
}
// Router for login
func LoginRouter() *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/login", login).Methods("Get")
	return router
}
// Router for Register
func RegisterRouter() *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/register", register).Methods("Get")
	return router
}
// Router for GetTweets
func GetTweetsRouter() *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/getTweets", getTweets).Methods("Get")
	return router
}
// Router for SaveCurrentTweets
func saveCurrentFetchedTweetsRouter() *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/saveCurrentFetchedTweets", saveCurrentFetchedTweets).Methods("Get")
	return router
}
// Router for create tweet
func createTweetsRouter() *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/createTweet", createTweet).Methods("Get")
	return router
}
// Router for surfacing tweets
func navigateTweetsRouter () *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/navigateTweets", navigateTweets).Methods("Get")
	return router
}
// Register Unit Test
func TestRegister(t *testing.T){
	bodyReader := strings.NewReader(`{"Email": "mohd.amr@hotmail.com", "Password": "password1"}`)
	request, _ := http.NewRequest("GET", "/register", bodyReader)
	response := httptest.NewRecorder()
	RegisterRouter().ServeHTTP(response,request)
	assert.Equal(t, 200, response.Code, "OK response is expected ::")
}
// Login Unit Test
func TestLogin(t *testing.T){
	bodyReader := strings.NewReader(`{"Email": "mohd.amr@hotmail.com", "Password": "password1"}`)
	request, _ := http.NewRequest("GET", "/login", bodyReader)
	response := httptest.NewRecorder()
	LoginRouter().ServeHTTP(response,request)
	assert.Equal(t, 200, response.Code, "OK response is expected ::")
}
// GetTweets Unit Test
func TestGetTweets(t *testing.T){
	bodyReader := strings.NewReader(`{}`)
	request, _ := http.NewRequest("GET", "/getTweets", bodyReader)
	doAuthForTest(request)
	response := httptest.NewRecorder()
	GetTweetsRouter().ServeHTTP(response,request)
	assert.Equal(t, 200, response.Code, "OK response is expected ::")
}
// Save Current Fetched Tweets Unit Test
func TestSaveCurrentFetchedTweets(t *testing.T){
	bodyReader := strings.NewReader(`{}`)
	request, _ := http.NewRequest("GET", "/saveCurrentFetchedTweets", bodyReader)
	doAuthForTest(request)
	response := httptest.NewRecorder()
	saveCurrentFetchedTweetsRouter().ServeHTTP(response,request)
	assert.Equal(t, 200, response.Code, "OK response is expected ::")
}
// Create Tweet Unit Test
func TestCreateTweets(t *testing.T){
	bodyReader := strings.NewReader(`{"Description": "Hakona Mattata"}`)
	request, _ := http.NewRequest("GET", "/createTweet", bodyReader)
	doAuthForTest(request)
	response := httptest.NewRecorder()
	createTweetsRouter().ServeHTTP(response,request)
	assert.Equal(t, 200, response.Code, "OK response is expected ::")
}
// Surfacing Tweets Unit Test
func TestNavigateTweets(t *testing.T){
	bodyReader := strings.NewReader(`{"limit": "5", "offset": "3"}`)
	request, _ := http.NewRequest("GET", "/navigateTweets", bodyReader)
	doAuthForTest(request)
	response := httptest.NewRecorder()
	navigateTweetsRouter().ServeHTTP(response,request)
	assert.Equal(t, 200, response.Code, "OK response is expected ::")
}
